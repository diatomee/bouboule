const Import = {
	props: {accounts: Array},
	data: function () {
		return {
			result: ''
		}
	},
	template: `
		<section>
			<h2>Importer</h2>
			<p>Permet d’importer les données d’un fichier bouboule.data. <strong>Attention&nbsp;: cet import écrasera toutes données actuellement présentes.</strong></p>
			<input type="file" @change="upload">
			<p if="result !== ''">{{ result }}</p>
		</section>
	`,
	methods: {
		upload: function (res) {
			res = res.target
			let fileList = res.files, file = fileList[0], reader = new FileReader(), data
			reader.onload = e => {
				data = reader.result
				const isJsonString = str => {
					try {
						JSON.parse(str)
					} catch (e) {
						return false
					}
					return true
				}
				if (isJsonString(data)) {
					data = JSON.parse(data)
					console.log(data)
					if(data.hasOwnProperty('accounts') && data.hasOwnProperty('notes')) {
						setLS(data)
						this.result = 'Import effectué. Actualiser la page pour voir les changements.'
					} else this.result = 'Fichier incorrect. Clef(s) manquante(s) dans le fichier.'
				} else this.result = 'Fichier incorrect. Le contenu du fichier n’est pas du JSON bien formé.'
			}
			reader.readAsText(file)
		}
	}
}
