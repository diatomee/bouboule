const Transfer = {
	props: {accounts: Array},
	data: function () {
		return {
			selectedOut: this.accounts[0].id,
			selectedIn: this.accounts[1].id,
			label: '',
			amount: '',
			result: ''
		}
	},
	template: `
		<section>
			<h2>Effectuer un virement</h2>
			De
			<select v-model="selectedOut" @change="updateSelIn">
				<option v-for="a of accounts" :value="a.id">
					{{ a.label }}
				</option>
			</select>
			vers
			<select v-model="selectedIn" @change="updateSelOut">
				<option v-for="a of accounts" :value="a.id">
					{{ a.label }}
				</option>
			</select>
			<br><br>
			<input placeholder="Libellé" v-model.trim="label">
			<input placeholder="Montant" style="width: 130px;" v-model.trim="amount">
			<span id=addAccount class="icon add" @click="add"></span>
			<div style="margin-top:10px;" v-if="result !== ''" v-html="result"></div>
		</section>
	`,
	methods: {
		updateSelIn: function () {
			if (this.selectedIn !== this.selectedOut) return
			for (let a of this.accounts) {
				if (a.id !== this.selectedOut) {
					this.selectedIn = a.id
					return
				}
			}
		},
		updateSelOut: function () {
			if (this.selectedIn !== this.selectedOut) return
			for (let a of this.accounts) {
				if (a.id !== this.selectedIn) {
					this.selectedOut = a.id
					return
				}
			}
		},
		add: function () {
			const amount = formatAmount(this.amount)
			if (!amount || amount == 0) {
				this.result = '<span class=error>Montant manquant, nul ou incorrect.</span>'
				return
			}
			operationID ++
			const operationOut = {
				id: operationID,
				label: this.label != '' ? this.label : 'Virement',
				amount,
				isOutput: true,
				state: 0
			}
			operationID ++
			const operationIn = {
				id: operationID,
				label: this.label != '' ? this.label : 'Virement',
				amount,
				isOutput: false,
				state: 0
			}
			const accountOut = this.accounts[this.accounts.findIndex(a => a.id == this.selectedOut)]
			const accountIn = this.accounts[this.accounts.findIndex(a => a.id == this.selectedIn)]
			addOperation(accountOut, operationOut)
			addOperation(accountIn, operationIn)
			this.result = '<span class=success>Opérations créées.</span>'
		}
	}
}
