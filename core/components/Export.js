const Export = {
	props: {accounts: Array},
	data: function () {
		return {
			selected: ''
		}
	},
	template: `
		<section>
			<h2>Exporter</h2>
			<h3>Le stockage local du navigateur</h3>
			<p>Permet une sauvegarde complète des données de Bouboule, pour un import futur. <strong>Idéalement, il convient d’effectuer cette sauvegarde après toutes modifications apportées, avant de quitter Bouboule.</strong></p>
			<div class="btSave" @click="save">Sauvegarder</div>
			<h3>Les opérations d’un mois</h3>
			<p>Permet d’obtenir un document HTML contenant toutes les opérations du mois choisi, pour tous les comptes. Attention&nbsp;: Bouboule fusionne automatiquement les opérations ayant plus de 5 ans.</p>
			<div v-if="getMonths.length > 0">
				<select v-model="selected">
					<option v-for="m of getMonths" :value="m">
						{{ m }}
					</option>
				</select>
				<div class="btSave" @click="download">Télécharger</div>
			</div>
			<div v-else><em>Aucune opération validée pour le moment.</em></div>
		</section>
	`,
	computed: {
		getMonths: function () {
			let months = [], month
			for (let a of this.accounts) {
				for (let o of a.ops) {
					if (o.state !== 0) {
						month = o.date.substring(3)
						if (months.indexOf(month) === -1) months.push(month)
					}
				}
			}
			this.selected = months[0]
			return months
		}
	},
	methods: {
		save: function () {
			saveFile(JSON.stringify(d), 'bouboule.data', 'application/json')
		},
		download: function () {
			const monthsLetters = [
				'Janvier', 'Février', 'Mars', 'Avril',
				'Mai', 'Juin', 'Juillet', 'Août',
				'Septembre', 'Octobre', 'Novembre', 'Décembre'
			]
			let month = monthsLetters[parseInt(this.selected.split('/')[0]) - 1]
			let year = this.selected.split('/')[1]
			
			let balances = []
			let totalBalances = 0
			for (let a of this.accounts) {
				let operations = []
				let resumeOps = [0, 0]
				let balance = parseFloat(a.initAmount)
				for (let o of a.ops) {
					o.amount = parseFloat(o.amount)
					if (
						o.state === 1 &&
						o.date.split('/').reverse().join('') < this.selected.split('/').reverse().join('') + 31
					) {
						balance += o.isOutput ? - o.amount : o.amount
						if (o.date.substring(3) === this.selected) {
							operations.push(o)
							if (o.isOutput) {
								resumeOps[1] += o.amount
							} else resumeOps[0] += o.amount
						}
					}
				}
				totalBalances += balance
				balances.push({label: a.label, balance, operations, resumeOps})
			}

			let lines1 = []
			let lines2 = []
			for (b of balances) {
				lines1.push([
					'<div class=l>',
						'<div>', b.label, '</div>',
						'<div class=s>', prettyAmount(b.balance.toFixed(2)), '</div>',
					'</div>'
				].join(''))
				if (b.operations.length > 0) {
					let resume = ''
					if (b.resumeOps[0] === 0) {
						resume = '-' + b.resumeOps[1]
					} else if (b.resumeOps[1] === 0) {
						resume = b.resumeOps[0]
					} else {
						resume = [
							'<span title="Somme des crédits">',
							prettyAmount(b.resumeOps[0].toFixed(2)),
							'</span> - <span title="Somme des débits">',
							prettyAmount(b.resumeOps[1].toFixed(2)),
							'</span> = <span title="Différence entre crédits et débits\n(inclus les virements possibles de ou vers mes autres comptes)">',
							prettyAmount((b.resumeOps[0] - b.resumeOps[1]).toFixed(2)),
							'</span>'
						].join('')
					}
					b.resumeOps[0] -b.resumeOps[1]
					lines2.push('<h3>' + b.label + ' (' + resume + ')</h3>')
					for (let o of b.operations) {
						lines2.push([
							'<div class=l>',
								'<div>',
									o.date.split('/')[0], '. ',
									o.label,
								'</div>',
								'<div class="s', o.isOutput ? ' o': ' i', '">',
									prettyAmount(o.amount.toFixed(2)),
								,'</div>',
							'</div>'
						].join(''))
					}
				}
			}
			let dateGenerate = today().reverse().join('/')
			let template = [
				'<!doctype html>',
				'<html lang="fr">',
					'<head>',
						'<meta charset="utf-8">',
						'<title>', month + ' ' + year, '</title>',
						'<style>',
							'body{margin:0 auto;padding:30px;max-width:650px;font-family:monospace;opacity:.7;}',
							'h1{font-size:1.7em;border:4px double;padding:10px 15px;display:inline-block;}',
							'h2{margin-top:20px;}',
							'.l{',
								'display:grid;',
								'grid-template-columns:calc(100% - 160px) 150px;',
								'grid-column-gap:10px;',
								'align-items:center;',
								'padding:4px 6px;',
								'border-bottom:1px dashed #ccc;',
							'}',
							'.l:hover{background:#eee;cursor:default;}',
							'.s{text-align:right;font-weight:800;}',
							'.i{color:green;}',
							'.o{color:red;}',
						'</style>',
					'</head>',
					'<body>',
						'<h1>', month + ' ' + year, '</h1>',
						'<p><em>Généré par Bouboule le <b>', dateGenerate, '</b>.</em></p>',
						'<h2>Solde',
							this.accounts.length > 1 ? 's' : '', ' (', prettyAmount(totalBalances),
						')</h2>',
						lines1.join(''),
						'<h2>Opérations</h2>',
						lines2.join(''),
					'</body>',
				'</html>'
			].join('')

			const name = this.selected.split('/').reverse().join('-')
			saveFile(template, name + '.html', 'text/html')
		}
	}
}
