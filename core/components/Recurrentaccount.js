const Recurrentaccount = {
	props: {a: Object},
	components: {Recurrentoperation},
	template: `
		<div v-if="a.recs.length > 0">
			<h3>{{ a.label }}</h3>
			<Recurrentoperation :a="a" :o="o" v-for="o of a.recs" :key="o.id"></Recurrentoperation>
		</div>
	`
}
