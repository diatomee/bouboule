const Operation = {
	props: {a: Object, o: Object},
	data: function () {
		return {}
	}, 
	template: `
		<div class=line>
			<div class=label @click="changeLabel">{{ o.label }}</div>
			<div class=amount  @click="changeAmount" :class="type" v-html="prettyfy"></div>
			<div class=date style="text-align:center;">{{ o.date ? o.date : ''}}</div>
			<div class=state>
				<span class=icon :class="state" @click="toggleState"></span>
			</div>
		</div>
	`,
	computed: {
		type: function () {
			return this.o.isOutput ? 'output' : 'input'
		},
		state: function () {
			return this.o.state === 0 ? 'wait' : 'ok'
		},
		prettyfy: function () {
			return prettyAmount(this.o.amount.toFixed(2))
		}
	},
	methods: {
		changeLabel: function () {
			if (this.o.state === 1) return
			const label = prompt("Libellé :", this.o.label)
			if (!label || label.trim() === '') return
			this.o.label = label
			setLS(d)
		},
		changeAmount: function () {
			if (this.o.state === 1) return
			const amount = prompt("Montant\n(0 supprime l'opération) :", this.o.amount.toString().split('.').join(','))
			if (!amount || amount.trim() === '') return
			if (amount == 0) {
				rmOperation(this.a, this.o)
				return
			}
			const newAmount = formatAmount(amount)
			if (!newAmount) return
			this.o.amount = newAmount
			setLS(d)
		},
		toggleState: function () {
			if (this.o.state === 1) return
			const date = prompt("Opération effectuée le :\n(JJ/MM/AAAA)", today().reverse().join('/'))
			console.log(date)
			if (!date) return
			if (!formatDate(date)) {
				alert('Date incorrecte.')
				return
			}
			this.o.date = date
			this.o.state = 1
			setLS(d)
			sortOperations(this.a)
		}
	}
}
