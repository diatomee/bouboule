const Account = {
	props: {accounts: Array, a: Object},
	components: {Addoperation, Operation},
	data: function () {
		return {}
	}, 
	template: `
		<section>
			<h2 @click="changeLabel">{{ a.label }}</h2>
			<p>
				Solde&nbsp;: <span @click="changeInitAmount" v-html="balanceText"></span>
				<span v-if="a.maxAmount" @click="changeMaxAmount">
					·&nbsp;Plafond&nbsp;: <span v-html="prettyfy"></span>
				</span>
			</p>
			<Addoperation :a="a"></Addoperation>
			<section>
				<Operation :a ="a" :o="o" v-for="o of a.ops" :key="o.id"></Operation>
			</section>
		</section>
	`,
	computed: {
		balanceText: function () {
			let
        		previBalance = parseFloat(this.a.initAmount),
				balance = parseFloat(this.a.initAmount)
			for (let o of this.a.ops) {
				o.amount = parseFloat(o.amount)
				if (o.state === 1) {
					balance += o.isOutput ? - o.amount : o.amount
				}
				previBalance += o.isOutput ? - o.amount : o.amount
			}
			if (balance === previBalance) return '<b>' + prettyAmount(balance.toFixed(2)) + '</b>'
			return '<b>' + prettyAmount(balance.toFixed(2)) + '</b> (' + prettyAmount(previBalance.toFixed(2)) + ')'
		},
		prettyfy: function () {
			return prettyAmount(this.a.maxAmount)
		}
	},
	methods: {
		changeLabel: function () {
			const label = prompt("Libellé du compte :", this.a.label).trim()
			if (!label || label === '') return
			for (a of this.accounts) {
				if (a.label === label && a.id !== this.a.id) {
					alert("Ce libellé existe déjà.")
					return
				}
			}
			this.a.label = label
		},
		changeInitAmount: function () {
			const amount = prompt("Montant initial du compte\n('suppr' supprime le compte) :", this.a.initAmount.toString().split('.').join(','))
			if (!amount || amount === '') return
			if (amount == "suppr") {
				rmAccount(this.a)
				return
			}
			const newAmount = formatAmount(amount)
			if (!newAmount) return
			this.a.initAmount = newAmount
		},
		changeMaxAmount: function () {
			let amount = prompt("Plafond du compte\n('0' supprime le plafond) :", this.a.maxAmount.toString().split('.').join(','))
			if (!amount || amount === '') return
			if (amount == 0) {
				rmMaxAmount(this.a)
				return
			}
			const newAmount = formatAmount(amount)
			if (!newAmount) return
			this.a.maxAmount = newAmount
		},
	}
}
