const Addoperation = {
	props: {a: Object},
	data: function () {
		return {
			label: '',
			type: 'out',
			amount: ''
		}
	},
	template: `
		<section style="margin-bottom: 5px;">
			<input placeholder="Libellé de l'opération" v-model.trim="label">
			<span class="icon" :class="type" @click="toggle"></span>
			<input
				placeholder="Montant"
				style="width: 150px;padding-left: 30px;"
				v-model="amount"
				@keydown.109.prevent="getOut"
				@keydown.107.prevent="getIn"
			>
			<span id=addAccount class="icon add" @click="add"></span>
		</section>
	`,
	methods: {
		toggle: function () {
			this.type = this.type === 'out' ? 'in' : 'out'
		},
		getOut: function () {
			this.type = 'out'
		},
		getIn: function () {
			this.type = 'in'
		},
		add: function () {
			if (this.label === '') return
			const amount = formatAmount(this.amount)
			if (!amount) return
			let count = 30
			for (let o of this.a.ops) {
				count --
				if (
					o.label === this.label &&
					o.amount === amount &&
					o.isOutput === (this.type === 'out')
				) {
					alert('Avertissement : Dans les 30 dernières opérations de ce compte, il y en a une identique (même libellé, même montant).')
				}
				if (count === 0) break
			}
			operationID ++
			const operation = {
				id: operationID,
				label: this.label,
				amount,
				isOutput: this.type === 'out',
				state: 0
			}
			addOperation(this.a, operation)
			this.label = ''
			this.amount = ''
		}
	}
}
