const Recurrentform = {
	props: {accounts: Array},
	data: function () {
		return {
			selected: this.accounts[0].id,
			label: '',
			type: 'out',
			amount: '',
			freq: '',
			end: '',
			result: ''
		}
	},
	template: `
		<div>
			Compte où s’effectue l’opération&nbsp;:
			<select v-if="accounts.length > 1" v-model="selected" @focus="rmMsg">
				<option v-for="a of accounts" :value="a.id">
					{{ a.label }}
				</option>
			</select>
			<span v-else><b>{{ accounts[0].label }}</b></span>
			<br><br>
			<input
				placeholder="Libellé de l'opération"
				v-model.trim="label"
				@focus="rmMsg"
			>
			<span class="icon" :class="type" @click="toggle"></span>
			<input
				placeholder="Montant"
				style="width: 110px;padding-left: 30px;"
				v-model="amount"
				@keydown.109.prevent="getOut"
				@keydown.107.prevent="getIn"
				@focus="rmMsg"
			>
			<input
				placeholder="Fréquence"
				style="width: 180px;"
				v-model.trim="freq"
				@focus="rmMsg"
			>
			<input
				placeholder="Fin (JJ/MM/AAAA)"
				style="width: 140px;"
				v-model.trim="end"
				@focus="rmMsg"
			>
			<span id=addRecurrence class="icon add" @click="add"></span>
			<div style="margin-top:10px;" v-if="result !== ''" v-html="result"></div>
		</div>
	`,
	methods: {
		rmMsg: function () {
			this.result = ''
		},
		toggle: function () {this.type = this.type === 'out' ? 'in' : 'out'},
		getOut: function () {this.type = 'out'},
		getIn: function () {this.type = 'in'},
		add: function () {
			let result2 = '', fullDate
			if (this.label === '') {
				this.result = '<span class=error>Libellé manquant.</span>'
				return
			}
			const amount = formatAmount(this.amount)
			if (!amount) {
				this.result = '<span class=error>Montant manquant ou incorrect.</span>'
				return
			}
			if (this.freq === '') {
				this.result = '<span class=error>Fréquence manquante.</span>'
				return
			}

			if (
				this.end !== '' &&
				(!formatDate(this.end) || this.end.split('/').reverse().join('') <= today().join(''))
			) {
				this.result = '<span class=error>Date de fin incorrecte, passée ou égale à aujourd’hui.</span>'
				return
			}

			//vérif de la fréquence
			const type = freqType(this.freq)
			switch (type) {
				case 'annual':
					const j = this.freq.split('/')[0]
					const m = this.freq.split('/')[1]
					if (m === '02') {
						if (/^29|30|31$/.test(j)) {
							result2 = "Ajustement de la fréquence&nbsp;: Le jour est reporté au 28 pour février, car Bouboule ne tient pas compte des années bissextiles pour les récurrences."
							this.freq = '28/02'
							j = 28
						}
					} else if (['04','06','09','11'].indexOf(m) !== -1) {
						if (j === '31') {
							result2 = "Ajustement de la fréquence&nbsp;: Le jour est reporté au 30 car le mois indiqué (" + m + ") n’a pas 31 jours."
							this.freq = '30/' + m
							j = 30
						}
					}
					fullDate = [today()[0], m, j].join('-')
					if (diffDates(fullDate, today().join('-')) <= 0) {
						//si < 0 on ajoute une année
						fullDate = [today()[0] + 1, m, j].join('-')
					}
					break
				case 'mensual':
					if (/^29|30|31$/.test(this.freq)) {
						result2 = "Fréquence&nbsp;: Le dernier jour du mois sera utilisé si le mois dispose de moins de jours qu’indiqué."
					}
					const j1 = this.freq
					fullDate = [today()[0], today()[1], j1].join('-')
					const diff = diffDates(fullDate, today().join('-')) //diff en jours
					if (diff <= 0) {
						//si < 0 on ajoute un mois et si mois > 12 on ajoute 1 année
						let numMonth = parseInt(today()[1]) + 1
						let numYear = parseInt(today()[0])
						if (numMonth === 13) {
							numYear = today()[0] + 1
							numMonth = 1
						}
						if (numMonth < 10) numMonth = '0' + numMonth
						fullDate = [numYear, numMonth, j1].join('-')
					}
					break
				case 'error':
					this.result = '<span class=error>Fréquence incorrecte.</span>'
					return
			}

			recurrenceID ++
			const recurrence = {
				id: recurrenceID,
				label: this.label,
				amount,
				isOutput: this.type === 'out',
				freq: this.freq,
				fullDate,
				type,
				end: this.end
			}
			if (this.selected === '' || this.accounts.length === 1) {
				this.selected = this.accounts[0].id
			}
			const account = this.accounts[this.accounts.findIndex(a => a.id === this.selected)]
			addRecurrence(account, recurrence)
			this.label = ''
			this.amount = ''
			this.freq = ''
			this.end = ''
			this.result = '<span class=success>Récurrence ajoutée.</span> ' + result2
		}
	}
}