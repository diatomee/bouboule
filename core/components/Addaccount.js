const Addaccount = {
	props: {accounts: Array},
	data: function () {
		return {
			label: '',
			initAmount: '',
			maxAmount: '',
			result: ''
		}
	},
	template: `
		<section>
			<h2>Ajouter un compte</h2>
			<input placeholder="Libellé du compte" v-model.trim="label" @focus="rmMsg">
			<input placeholder="Somme initiale" style="width: 130px;" v-model.trim="initAmount" @focus="rmMsg">
			<input placeholder="Plafond" style="width: 130px;" v-model.trim="maxAmount" @focus="rmMsg">
			<span id=addAccount class="icon add" @click="add"></span>
			<div style="margin-top:10px;" v-if="result !== ''" v-html="result"></div>
		</section>
	`,
	methods: {
		rmMsg: function () {
			this.result = ''
		},
		add: function () {
			if (this.label === '') {
				this.result = '<span class=error>Libellé manquant.</span>'
				return
			}
			for (let a of this.accounts) {
				if (a.label === this.label) {
					this.result = '<span class=error>Libellé de compte déjà utilisé.</span>'
					return
				}
			}
			const initAmount = formatAmount(this.initAmount)
			const maxAmount = formatAmount(this.maxAmount)
			if (!initAmount) {
				this.result = '<span class=error>Montant initial manquant.</span>'
				return
			}
			accountID ++
			const account = {
				id: accountID,
				label: this.label,
				initAmount,
				maxAmount,
				ops: [],
				recs: []
			}
			addAccount(account)
			this.label = ''
			this.amount = ''
			this.max = ''
			this.result = '<span class=success>Compte ajouté.</span>'
		}
	}
}
