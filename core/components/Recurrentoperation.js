const Recurrentoperation = {
	props: {a: Object, o: Object},
	template: `
		<div class=line1>
			<div class=label @click="changeLabel">{{ o.label }}</div>
			<div 
				@click="changeAmount"
				class=amount :class="o.isOutput ? 'output' : 'input'"
				v-html="prettyfy"
			>
			</div>
			<div class=freq @click="changeFreq" :title="o.fullDate.split('-').reverse().join('/')">{{ o.freq }}</div>
			<div class=end @click="changeEnd">{{ o.end !== '' ? o.end : '-' }}</div>
		</div>
	`,
	computed: {
		prettyfy: function () {
				return prettyAmount(this.o.amount)
			}
	},
	methods: {
		changeLabel: function () {
			const label = prompt("Libellé :", this.o.label)
			if (!label || label.trim() === '') return
			this.o.label = label.trim()
		},
		changeAmount: function () {
			const amount = prompt("Montant\n(0 supprime l’opération) :", this.o.amount.toString().split('.').join(','))
			if (!amount || amount.trim() === '') return
			if (amount == 0) {
				rmRecurrence(this.a, this.o)
				return
			}
			const newAmount = formatAmount(amount)
			if (!newAmount) return
			this.o.amount = newAmount
		},
		changeFreq: function () {
			const freq = prompt("Fréquence :", this.o.freq)
			if (!freq || freq.trim() === '') return
			alert('Il est pour le moment impossible de changer une fréquence. Cette fonctionnalité viendra prochainement.')
		},
		changeEnd: function () {
			const end = prompt("Date de fin\n(facultative) :", this.o.end).trim()
			if (!end) return
			if (end === '') {
				this.o.end = ''
				return
			}
			if (!formatDate(end) || end.split('/').reverse().join('') <= t.join('')) {
				alert('Date de fin incorrecte, passée ou égale à aujourd’hui.')
				return
			}
			this.o.end = end
		}
	}
}
