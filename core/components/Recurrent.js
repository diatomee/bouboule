const Recurrent = {
	props: {accounts: Array},
	data: function () {
		return {
			help: false
		}
	},
	components: {Recurrentform, Recurrentaccount},
	template: `
		<section>
			<h2>Opérations récurrentes<sup style="font-size:.6em;padding-left:5px;cursor:pointer;" @click="toggleHelp">?</sup></h2>
			<div v-if="help">
				<p>Cette section permet l’automatisation des opérations répétitives dont le montant ne varie pas souvent (assurances, loyers, crédits). La date de fin est facultative. La syntaxe de la fréquence est particulière&nbsp;:</p>
				<ul>
					<li>Annuelle&nbsp;: <code>JJ/MM</code>, exemple&nbsp;: <code>02/11</code></li>
					<li>Mensuelle&nbsp;: <code>JJ</code>, exemple&nbsp;: <code>28</code></li>
					<!--<li>Hebdomadaire&nbsp;: <code>jour</code>, exemple&nbsp;: <code>lundi</code></li>-->
				</ul>
				<p>Les fréquences hebdomadaires et multiples ne sont pas prises en charge. Cela viendra peut-être un jour.</p>
				<!--<p>Fonctionnalité à venir&nbsp;: Il est aussi possible de définir plusieurs frèquences en les séparant d’un espace (<code>30/01 15/07</code> pour une fréquence bi-annuelle) ou d’utiliser un multiplicateur pour une fréquence régulière (<code>30/01 x3</code> pour une fréquence trimestrielle, cela revient à écrire <code>30/01 30/04 30/07 30/10</code>).</p>-->
			</div>
			<Recurrentform :accounts="accounts"></Recurrentform>
			<br>
			<Recurrentaccount :a="a" v-for="a of accounts" :key="a.id"></Recurrentaccount>
		</section>
	`,
	methods: {
		toggleHelp: function () {
			this.help = !this.help
		}
	}
}
