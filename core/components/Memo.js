const Memo = {
	props: {notes: String},
	data: function () {
		return {
			n: this.notes
		}
	},
	template: `
		<section>
			<h2>Notes</h2>
			<textarea placeholder="Mes notes" v-model.trim="n" @input="update"></textarea>
		</section>
	`,
	methods: {
		update: function () {
			this.$root.notes = this.n
			d.notes = this.n
			setLS(d)
		}
	}
}
