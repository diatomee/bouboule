const LSName = 'bouboule'

const today = () => {
	const t = new Date()
	let dd = t.getDate()
	let mm = t.getMonth() + 1 //January is 0!
	if (dd < 10) dd = '0' + dd
	if (mm < 10) mm = '0' + mm
	return [t.getFullYear(), mm, dd]
}

const setLS = d => localStorage.setItem(LSName, JSON.stringify(d))

const saveFile = (d, filename, type) => {
	const file = new Blob([d], {type})
	if (window.navigator.msSaveOrOpenBlob) // IE10+
		window.navigator.msSaveOrOpenBlob(file, filename)
	else { // Others
		const a = document.createElement("a")
		const url = URL.createObjectURL(file)
		a.href = url
		a.download = filename
		document.body.appendChild(a)
		a.click()
		setTimeout(function() {
			document.body.removeChild(a)
			window.URL.revokeObjectURL(url)
		}, 0)
	}
}

const addAccount = account => {
	d.accounts.push(account)
	d.accountID = accountID
	setLS(d)
}
const rmAccount = account => {
	const i = d.accounts.findIndex(a => a.id === account.id)
	d.accounts.splice(i, 1)
	setLS(d)
}
const rmMaxAmount = account => {
	const i = d.accounts.findIndex(a => a.id === account.id)
	d.accounts[i].maxAmount = undefined
}

const addOperation = (account, operation) => {
	account.ops.unshift(operation)
	d.operationID = operationID
	setLS(d)
}
const rmOperation = (account, operation) => {
	const i = d.accounts.findIndex(a => a.id === account.id)
	const j = d.accounts[i].ops.findIndex(o => o.id === operation.id)
	d.accounts[i].ops.splice(j, 1)
	setLS(d)
}
const sortOperations = account => { //cette fonction ne s'utilise que quand on change le statut d'une opération
	function compare(a,b) {
		if (a.state < b.state) return -1
		if (a.state > b.state) return 1
		if (!a.date) return 0
		const a1 = a.date.split('/').reverse().join('')
		const a2 = b.date.split('/').reverse().join('')
		if (a1 < a2) return 1
		if (a1 > a2) return -1
		return 0
	}
	account.ops.sort(compare)
	setLS(d)
}

const addRecurrence = (account, recurrence) => {
	account.recs.push(recurrence)
	d.recurrenceID = recurrenceID
	//tri récurrences du compte par fullDate (les plus proches en premier)
	function compare(a,b) {
		if (a.fullDate < b.fullDate) return -1
		if (a.fullDate > b.fullDate) return 1
		return 0
	}
	account.recs.sort(compare)
	setLS(d)
}
const rmRecurrence = (account, recurrence) => {
	const i = d.accounts.findIndex(a => a.id === account.id)
	const j = d.accounts[i].recs.findIndex(r => r.id === recurrence.id)
	d.accounts[i].recs.splice(j, 1)
	setLS(d)
}

const freqType = freq => {
	if (/^(0[1-9]|[12][0-9]|3[01]){1}\/(0[1-9]|1[0-2]){1}$/.test(freq)) return 'annual'
	if (/^(0[1-9]|[12][0-9]|3[01]){1}$/.test(freq)) return 'mensual'
	//const days = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']
	//if (days.indexOf(freq) !== -1) return 'weekly'
	return 'error'
}

const diffDates = (date1, date2) => {
	const timeDiff = new Date(date1).getTime() - new Date(date2).getTime()
	return Math.ceil(timeDiff / (1000 * 3600 * 24))
}

const formatAmount = amount => {
	if (!/^(\d+)(?:([\.,])(\d{1,2}))*$/.test(amount)) return
	return parseFloat(amount.toString().replace(',', '.')).toFixed(2)
}

const formatDate = date => {
	const elts = date.split('/')
	if (elts.length !== 3) return
	const d = elts[0], m = elts[1] - 1, y = elts[2], dateObj = new Date(y, m, d)
	if (dateObj.getFullYear() != y || dateObj.getMonth() != m || dateObj.getDate() != d) return
	return true
}

const prettyAmount = amount => {
	let parts = amount.toString().split(".")
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, "<span style='font-size:.6em;'>&nbsp;</span>")
	return parts.join(',')
}


// INIT

//LS compare and adapt
if (localStorage.getItem(LSName) === null) {
	localStorage.setItem(LSName, JSON.stringify({
		accounts: [],
		notes: 'Cette zone permet de noter tout ce que l’on veut.'
	}))
}
const d = JSON.parse(localStorage.getItem(LSName))

let accountID = d.accountID || 0
let operationID = d.operationID || 0
let recurrenceID = d.recurrenceID || 0

//Bac à sable, pour les tests
const sandbox = () => {
	recurrenceID ++
	addRecurrence(d.accounts[0], {
		id: recurrenceID,
		label: 'yo',
		amount: '999.99',
		isOutput: false,
		freq: '22/02',
		//count: 4,
		end: ''
	})
}
//sandbox()
//Fin du bac à sable

//remove obsolete recurrences
for (let a of d.accounts) {
	for (let r of a.recs) {
		if (r.end && r.end.split('/').reverse().join('') < t.join('')) {
			rmRecurrence(a, r)
		}
	}
}

//vérif qu'une fullDate n'est pas passée, sinon on insère l'op et on recalcule fullDate
for (let a of d.accounts) {
	for (let r of a.recs) {
		if (diffDates(r.fullDate, today().join('-')) <= 0) {
			//on crée l'op
			operationID ++
			addOperation(a, {
				id: operationID,
				label: r.label,
				amount: r.amount,
				isOutput: r.isOutput,
				state: 0
			})
			//on change la fullDate
			let fullDate
			if (r.type === 'annual') {
				const j = r.freq.split('/')[0]
				const m = r.freq.split('/')[1]
				fullDate = [today()[0], m, j].join('-')
				if (diffDates(fullDate, today().join('-')) <= 0) {
					fullDate = [today()[0] + 1, m, j].join('-')
				}
			} else if (r.type === 'mensual') {
				const j = r.freq
				fullDate = [today()[0], today()[1], j].join('-')
				const diff = diffDates(fullDate, today().join('-'))
				if (diff <= 0) {
					let numMonth = parseInt(today()[1]) + 1
					let numYear = parseInt(today()[0])
					if (numMonth === 13) {
						numYear = today()[0] + 1
						numMonth = 1
					}
					if (numMonth < 10) numMonth = '0' + numMonth
					fullDate = [numYear, numMonth, j].join('-')
				}
			}
			r.fullDate = fullDate
		}
	}
}
//sort recs by fullDate
for (let a of d.accounts) {
	function compare(a,b) {
		if (a.fullDate < b.fullDate) return -1
		if (a.fullDate > b.fullDate) return 1
		return 0
	}
	a.recs.sort(compare)
}
setLS(d)