Bouboule est un gestionnaire de comptes simple, destiné principalement à un usage non professionnel.

L'outil remplace astucieusement l'utilisation de l'habituel tableur pour tenir sa comptabilité (Excel, Calc...). Il ne nécessite pas l'insertion de formules mathématiques et dispose d'une mise en page claire et moderne.

## Fonctionnalités

- Insertion de comptes (livret A, compte courant...) avec possibilité d'insérer une somme de départ
- Insertion d'opérations avec status (effectuée / en attente)
- Affichage du solde et du solde prévisionnel (si des opérations sont en attente)
- Insertion rapide de virements d'un compte à l'autre
- Renouvellement automatique d'opérations régulières (salaire, assurances, loyer...)
- Détail mois par mois enregistrable en HTML (et donc imprimable si besoin)
- Fonctionne dans un navigateur (pas de logiciel à installer)
- Opensource et gratuit